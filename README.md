## About this tutorial
This repo contains the information necessary to generate Wepy inputs for unbinding simulations. First, it is necessary to have CharmmGUI OpenMM outputs for your protein-ligand system of interest. Using these outputs, rst and pkl files containing information such as a topology, starting positions etc. are created. These files are then plugged into a Wepy main-script and used to initialize simulations. A sample CharmmGUI folder of the 1AJJ PDB is provided here.

Note; we are aware that the 1AJJ system is not a protein-ligand (un)binding system, but it was chosen for use here as it is uses very small, portable files and the following example simulations for tutorial purposes can be run quickly.

___

## Authors
Nicole Roussey(1), Duncan Boren(1), & Alex Dickson(1)(2)

(1). The Department of Biochemistry and Molecular Biology, Michigan State University, East Lansing, Michigan

(2). The Department of Computational Mathematics, Science, & Engineering, Michigan State University, East Lansing, Michigan

---

## Installation and Requirements

The softwares required can be installed with pip or conda. This may require updating pip to the latest version with **pip install --upgrade pip**. 

To install Wepy: 

**pip install wepy[all]**  

To make sure Wepy was successfully installed,  
**wepy --help**  

For more information on installing Wepy see the link provided below under Softwares.

To install OpenMM:  
**conda install -c omnia openmm**     

To make sure OpenMM was installed correctly,

**python -m simtk.testInstallation**

For OpenMM installation instructions please see: '[OpenMM Installation](http://docs.openmm.org/7.0.0/userguide/application.html)'

**As of March 2022** The latest Wepy is compatible with OpenMM v7.4.1 or v7.5+. Recomended to do **conda install -c omnia openmm=7.4.1** due to apparent incompatibility with v7.4.2.

---
## Contents of the Repository
In the wepy_tutorial directory you will find 9 provided files:

1. bash_README.sb
2. csh_make_pkl_files
3. bash_make_pkl_files.sb
4. make_pkl_files.py
5. get_char_dist.py
6. revo_char_dist.py
7. wepy_unbinding.py 
8. walker_pkl_reporter.py
9. resampled_data_dcd-maker.py

As well as 1 folder:
1. 1AJJcharmmgui.tar.xz


##### bash_README.sb

A bash version of the executable README file that is provided by CharmmGUI in the charmm-gui-####/openmm folder.

##### csh_make_pkl_files / bash_make_pkl_files.sb

csh: This file contains the code needed to run **make_pkl_files.py** to generate the system.pkl and topology.pkl files necessary for Wepy. 
bash: A bash version of the executable **csh_make_pkl_files** file. 

##### make_pkl_files.py

The script can be run by either **csh_make_pkl_files** or **bash_make_pkl_files.sb** to generate the system.pkl and topology.pkl files necessary for Wepy.

##### get_char_dist.py  

This is a Wepy main script that we use to run a 1 cycle simulation to determine the value for your systems characteristic distance.

##### revo_char_dist.py

This is a copy of the REVO resampler code that prints the value of your systems characteristic distance.

##### wepy_unbinding.py  

This is a Wepy main script that we use to run an unbinding simulation for your system of choice. The code is generalized and certain parameters may need to be changed depending on your system. Specific values for the 1AJJ example system are currently in place in this file. 

##### walker_pkl_reporter.py

An additional Wepy reporter code (saves .pkl files that can be used to restart simulations).

##### resampled_data_dcd-maker.py

This file can be used to generate a DCD for a trajectory in a resampled (or straightforward) Wepy simulation.

##### 1AJJcharmmgui.tar.xz

An example of CharmmGUI-OpenMM outputs that works with this tutorial. Folder must be opened for use.
Can be opened on a linux machine via: **tar -xf 1AJJcharmmgui.tar.xz**

## Generating Input Files

This tutorial requires a .PDB file representing a protein:ligand system of choice.

Register an account at CHARMM-GUI (https://www.charmm-gui.org/?doc=register). This can take up to 24 hours, so it may be helpful to complete this step some time in advance of when you plan to continue.
Using the CharmmGUI input assembler of choice, generate final output files for use with pure OpenMM (The Charmm-OpenMM option is not suitable for this tutorial) and download the provided tar.gz file.
Transfer your opened tar file (or the example provided here) to your location of interest **appropriate for running a short equilibration, production simulation, and Wepy simulation**.

1. To begin generating input files it is first necessary to utilize the **openmm_run.py** script. 
(This is located in the **charmm_gui-#####/openmm/** folder of a standard CharmmGUI output folder or the **charmm-gui-tarball/openmm** folder in the provided example.)

The **openmm_run.py** file is used by running the provided README file from CharmmGUI via:

**chmod +x README**

**csh README**

Note; Running this can take several hours. For the tutorial, **nstep** and **nstdcd** in the **step6_production.inp** file can be reduced by a factor of 10.

Note; Alternative to this, the provided **bash_README.sb** file can be **cp** from this tutorial folder into the **openmm** folder and run in place of the above step via:

**bash bash_README.sb**

This code runs a short equilibration of the system and a short production run. This production run generates .rst files. **The final .rst file, step6_10.rst, is a necessary Wepy input file**.
It is recommended to visualize the production run DCDs before moving on.

2. The next step is to generate **system.pkl** and **topology.pkl** pickle files, necessary Wepy input files. This is done by running the **csh_make_pkl_files** or the  **bash_make_pkl_files.sb** file, which calls the **make_pkl_files.py** script and passes in the necessary inputs.
These files can be used with either the example provided here or with your own CharmmGUI output folder.

2A. First **cp** the **csh_make_pkl_files** (or **bash_make_pkl_files.sb**) and the **make_pkl_files.py** into the **charmm_gui-#####/openmm/** folder of your CharmmGUI output folder or the **charmm-gui-tarball/openmm** folder in the provided example.

Note; if using your own CharmmGUI folder, make sure that the step numbers (ex: production = step 6) is consistant between your files and the **bash/csh_make_pkl_files** files.

2B. Then do:

**chmod +x csh_make_pkl_files**

**csh csh_make_pkl_files**

Note; Alternative to this, **bash_make_pkl_files.sb** file can be run in place of the above step via:

**bash bash_make_pkl_files.sb**

The **system.pkl** and **topology.pkl** pickle files should now be generated.

## Wepy unbinding simulation information

1. Outside of the CharmmGUI folder in your desired folder location, create a directory for running your Wepy simulations. From this tutorial repo, **cp** the **wepy_unbinding.py** and **walker_pkl_reporter.py** files into this folder.

2. In your new Wepy folder, make a directory named "inputs", **mkdir inputs**.
From the **charmm-gui-#####/openmm** or **chargmm-gui-tarball/openmm** folder, **cp** the "step4_input.pdb", "step_6_10.rst","system.pkl", and "topology.pkl" files into your inputs folder.

3. If using your own system and not the example provided, it is necessary to add in your own values for the parameters: char_dist, ligand_idxs, protein_idxs, and binding_site_idxs specific to your system in **wepy_unbinding.py**.

4. For systems other than the example provided, is necessary to determine the characteristic distance for use with REVO (the resampler used in this example), which is currently set to 0.3579 in **wepy_unbinding.py** for use with the 1AJJ example system. To generate this system-specific value (which is the mean of the distance matrix following one cycle of simulation), please see the REVO paper cited below or follow the **char_dist** steps below. To follow the tutorial with the example system, this step can be skipped.

____________________________

**char_dist generation**

The char_dist for the example system here has already been generated. The files used to do so are provided. To generate this value for the example system **cp** **get_char_dist.py** and **revo_char_dist.py** into your Wepy folder from above.

Note; this code as-is requires the use of GPUs. **Also, If you are doing this for your own system**, it is first necessary to change the ligand_idxs, protein_idxs, and binding_site_idxs to those relevant for your system in the **get_char_dist.py** file. 

Run as follows: **python get_char_dist.py 1 10000 48 1 1**

The **char_dist** will be printed.

The **outputs_char_dist** directory can be deleted once you have obtained the value.
____________________________


In its current form, this script uses the following noteworthy code:
- The CUDA platform
- Langevin integration
- The Wepy OpenMMRunner*
- The Wepy UnbindingDistance distance metric 
- The REVO Resampler
- The Wepy UnbindingBC boundary condition
- The GPU TaskMapper**
- An initial weight of 1/n_walkers

*In our script, enforce_box=True. This prewraps the positions in the periodic box prior to saving the positions. It is not necessary to set this to True.

**This TaskMapper is for use with (multiple) GPUs if available. It is not necessary to use this mapper. An alternative mapper (Mapper(), and relevant import) is commented out several lines above and can be used with other setups. For more information on this please see the Wepy docs.

The arguments to run **wepy_unbinding.py** are as follows (all ints):
1. n_cycles:    Number of cycles
2. n_steps:     Number of steps per cycle
3. n_walkers:   Number of walkers
4. n_workers:   Number of GPUs
5. run_num:     Run number (used for file naming purposes)

The output for this Wepy simulation is as follows:
1. A wepy.h5 file (as written, contains positions, box vectors, and velocities along with all other wepy warping/resampling etc simulation data) from the HDF5Reporter
2. pkl files (for restarting simulation from final cycle) from the walker_pkl_reporter
3. wepy.dash.org (interactive file for visualizing simulation progress) from the dashboard_reporter

To run a (very) short 5 trajectory test simulation:
**python wepy_unbinding.py 15 10000 5 1 1**

It is recommended to use this test simulation to generate a DCD file for visualization prior to submitting a longer run (to confirm that dynamics are running properly).
This can be done by doing **cp** on the **resampled_data_dcd-maker.py** file from this repo into your Wepy folder and running it.

The arguments to run **resampled_data_dcd-maker.py** and run line are as follows:
1. walker_idx: The walker you would to visualize (int)
2. pdb_path: The path to your systems pdb (**inputs/step4_input.pdb** if following the example)
3. wepy_path: The path to your Wepy HDF5 file (**outputs/wepy_run1.wepy.h5** if following the example)

**python resampled_data_dcd-maker.py 0 inputs/step4_input.pdb outputs/wepy_run1.wepy.h5**

Common issues: If using you own CharmmGUI files and the system immediately produces NaNs for positions, or dynamics do not look right, it is possible that the step numbers used in the **csh/bash_make_pkl_files** are not correct and the step number for the equilibration run were used by mistake. It is necessary to use the production run step number and corresponding inputs.

Argument values will vary from system to sytem but example values used for previous unbinding simulations are values such as:
1. n_cycles = 3000
2. n_steps = 10000
3. n_walkers = 48
4. n_workers = 8 (if available)
5. run_num = 1 (for first run)

Wall time of these parameters on V100 GPUs with a ~12,000 atom system = 2.33 days

## Wepy analysis

For more information on how to interact with wepy.h5 files and perform general analysis, please see '[the Wepy docs](https://adicksonlab.github.io/wepy/_api/modules.html)'. Basic file interaction is done in the **dcd-maker** code provided in this tutorial.

---

## References

##### Software Packages

'[Wepy](https://github.com/ADicksonLab/wepy)'

'[OpenMM](http://openmm.org)'

'[NumPy](https://numpy.org)'

'[h5py](https://www.h5py.org)'

##### Papers

'[Wepy: A Flexible Software Framework for Simulating Rare Events with Weighted Ensemble Resampling](https://pubs.acs.org/doi/10.1021/acsomega.0c03892)' Lotz, S., & Dickson, A., ACS Omega, 2020

'[REVO: Resampling of ensembles by variation optimization](https://aip.scitation.org/doi/10.1063/1.5100521)' Donyapour, N., Roussey, N., Dickson, A., AIP, 2019
