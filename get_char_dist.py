import pickle
import time

import sys
import numpy as np
import os
import os.path as osp

import simtk.openmm.app as omma
import simtk.openmm as omm
import simtk.unit as unit

import mdtraj as mdj
from copy import copy
from copy import deepcopy

from wepy.sim_manager import Manager
from revo_char_dist import REVOResampler
from wepy.resampling.distances.receptor import UnbindingDistance

from wepy.runners.openmm import UNIT_NAMES, GET_STATE_KWARG_DEFAULTS
from wepy.runners.openmm import OpenMMGPUWalkerTaskProcess, OpenMMRunner, OpenMMWalker, OpenMMState, gen_sim_state
from wepy.boundary_conditions.receptor import UnbindingBC
from wepy.reporter.hdf5 import WepyHDF5Reporter
from wepy.work_mapper.task_mapper import TaskMapper
from wepy.util.mdtraj import mdtraj_to_json_topology

from walker_pkl_reporter import WalkersPickleReporter
from wepy.reporter.dashboard import DashboardReporter
from wepy.reporter.openmm import OpenMMRunnerDashboardSection
from wepy.reporter.revo.dashboard import REVODashboardSection
from wepy.reporter.receptor.dashboard import UnbindingBCDashboardSection


import logging
# set logging threshold
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(message)s')

import socket
print(f'hostname: {socket.gethostname()}')

if sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print("arguments: n_cycles, n_steps, n_walkers, n_workers, run_num")
else:
    n_cycles = int(sys.argv[1])
    n_steps = int(sys.argv[2])
    n_walkers = int(sys.argv[3])
    n_workers = int(sys.argv[4])
    run_num = int(sys.argv[5])

# SETUP ------------------------------------------------------

# set the string identifier for the platform to be used by openmm
PLATFORM = 'CUDA'

PRESSURE = 1.0*unit.atmosphere
TEMPERATURE = 300.0*unit.kelvin
FRICTION_COEFFICIENT = 1/unit.picosecond
STEP_SIZE = 0.002*unit.picoseconds
SAVE_FIELDS = ('positions', 'box_vectors', 'velocities')

# make the outputs paths
inputs_dir = osp.realpath('./inputs/')
outputs_dir = osp.realpath(f'./outputs_char_dist/')

dashboard_path = osp.join(outputs_dir,'wepy.dash.org')
out_folder_pkl = osp.join(outputs_dir,f'pkl/')
os.makedirs(outputs_dir, exist_ok=True)
outfile = osp.join(outputs_dir, f'wepy_run{run_num}.wepy.h5')

# get input files
pdb_file = osp.join(inputs_dir,'step4_input.pdb')
pdbfile = mdj.load_pdb(pdb_file)
sys_pkl = osp.join(inputs_dir,'system.pkl')
top_pkl = osp.join(inputs_dir,'topology.pkl')
rst_file = osp.join(inputs_dir,'step6_10.rst')
# load input system and top pickles

with open(sys_pkl,'rb') as f:
    system = pickle.load(f)

with open(top_pkl,'rb') as f:
    omm_top = pickle.load(f)

# get positions and box vectors from an rst file                                               
with open(rst_file, 'r') as f:
    simtk_state = omm.XmlSerializer.deserialize(f.read())
    veloc = simtk_state.getVelocities()
    bv = simtk_state.getPeriodicBoxVectors()
    pos = simtk_state.getPositions()

# make an integrator object that is constant temperature
integrator = omm.LangevinIntegrator(TEMPERATURE, FRICTION_COEFFICIENT, STEP_SIZE)

# correct the box vectors to reflect the rst file, build the walker_state
system.setDefaultPeriodicBoxVectors(bv[0], bv[1], bv[2]) 
new_simtk_state = gen_sim_state(pos, system, integrator) 
walker_state = OpenMMState(new_simtk_state)

# selecting ligand and protein binding site atom indices for
# resampler and boundary conditions
protein_idxs = pdbfile.topology.select('protein')
lig_idxs = pdbfile.topology.select('resname "SO4"')
print("Ligand: {}".format(','.join([str(idx) for idx in lig_idxs])))
print("Protein: {}".format(','.join([str(idx) for idx in protein_idxs])))

binding_selection_idxs = np.ndarray.tolist(pdbfile.topology.select('resid 0 to 10'))

# get a json topology object
json_top = mdtraj_to_json_topology(pdbfile.top)

# set up the OpenMMRunner with your system
runner = OpenMMRunner(system, omm_top, integrator, platform=PLATFORM, enforce_box=True)
### Note: enforce_box=True pre-wraps the system in the periodic box

# Define the distance metric
unb_distance = UnbindingDistance(lig_idxs,
                                 binding_selection_idxs,
                                 walker_state)

# Define the resampler
char_dist = 1 
# NOTE: the char_dist value should be determined according to
# the method specified in:
# https://aip.scitation.org/doi/10.1063/1.5100521
merge_dist = 3*char_dist
resampler = REVOResampler(merge_dist=merge_dist,
                                  char_dist=char_dist,
                                  distance=unb_distance,
                                  init_state=walker_state)

# Define the boundary conditions
ubc = UnbindingBC(cutoff_distance=1.0, #nm
                  initial_state=walker_state,
                  topology=json_top,
                  ligand_idxs=lig_idxs,
                  receptor_idxs=protein_idxs)

# instantiate the units for the simulation
UNITS = UNIT_NAMES
units = dict(UNIT_NAMES)

# Define the mapper to be used for a SINGLE GPU
# mapper = Mapper()

# ALTERNATIVELY:
# create a work mapper for NVIDIA GPUs for a GPU cluster
mapper = TaskMapper(walker_task_type=OpenMMGPUWalkerTaskProcess,
                    num_workers=n_workers,
                    platform='CUDA',
                    device_ids=[i for i in range(0,n_workers)])

if __name__ == "__main__":

    print("Number of steps: {}".format(n_steps))
    print("Number of cycles: {}".format(n_cycles))
    # create the initial walkers
    init_weight = 1.0 / n_walkers

    init_walkers = [OpenMMWalker(walker_state, init_weight) for i in range(n_walkers)]

    hdf5_reporter = WepyHDF5Reporter(file_path=outfile, mode='w',
                                     save_fields=SAVE_FIELDS,
                                     resampler=resampler,
                                     boundary_conditions=ubc,
                                     topology=json_top,
                                     units=units)

    # Build the dashboard reporter
    openmm_dashboard_sec = OpenMMRunnerDashboardSection(runner)
    unb_bc_dashboard_sec = UnbindingBCDashboardSection(ubc)
    revo_dashboard_sec = REVODashboardSection(resampler)
    dashboard_reporter = DashboardReporter(file_path = dashboard_path,
                                           resampler_dash = revo_dashboard_sec,
                                           runner_dash = openmm_dashboard_sec,
                                           bc_dash = unb_bc_dashboard_sec)

    # make walker pkl reporter                
    pkl_reporter = WalkersPickleReporter(save_dir = out_folder_pkl,
                                         freq = 1,
                                         num_backups = 2)


    # define the reporters
    reporters = [hdf5_reporter, pkl_reporter, dashboard_reporter]

    # build the simulation manager
    sim_manager = Manager(init_walkers,
                          runner=runner,
                          resampler=resampler,
                          boundary_conditions=ubc,
                          work_mapper=mapper,
                          reporters=reporters)

    # make a number of steps for each cycle. In principle it could be
    # different each cycle
    steps = [n_steps for i in range(n_cycles)]
    
    # actually run the simulation
    print("Starting run: {}".format(0))
    sim_manager.run_simulation(n_cycles, steps)
    print("Finished run: {}".format(0))
        
    
    print("Finished first file")

    
