import sys
import numpy as np

import mdtraj as mdj
from wepy.hdf5 import WepyHDF5

from geomm.grouping import group_pair
from geomm.centering import apply_rectangular_pbcs
from wepy.util.util import traj_box_vectors_to_lengths_angles
from wepy.util.mdtraj import json_to_mdtraj_topology
from wepy.resampling.decisions.clone_merge import MultiCloneMergeDecision
from wepy.analysis.parents import (parent_panel, net_parent_table, resampling_panel,
                                   ancestors, resampling_panel)
from wepy.analysis.contig_tree import ContigTree

walker_idx = int(sys.argv[1])
pdb_path = sys.argv[2]
WEPY_PATH = sys.argv[3]

system_traj = mdj.load_pdb(pdb_path)
traj_top = system_traj.top

def make_dcd_for_walker(walker_idx):

    run_idx = 0
    decision_class = MultiCloneMergeDecision

    # open the hdf5 file
    wepy_h5 = WepyHDF5(WEPY_PATH, mode='r')
    wepy_h5.open()
    
    n_cycles = wepy_h5.num_run_cycles(0)
    wepy_h5.close()
    
    contigtree = ContigTree(wepy_h5,
                         decision_class=decision_class
                        )

    final_walkers = contigtree.final_trace()

    lineages = contigtree.lineages([(run_idx, walker_idx, n_cycles-1)], discontinuities=False)


    wepy_h5.open()
    trace_data = wepy_h5.get_trace_fields(lineages[0],
                                            ['positions','box_vectors'])

    uncentered_positions = trace_data['positions']
    unitcell_lengths, unitcell_angles = traj_box_vectors_to_lengths_angles(
        trace_data['box_vectors'])

    # make the trajectory for the walker
    traj = mdj.Trajectory(uncentered_positions, traj_top,
                          unitcell_lengths=unitcell_lengths,
                          unitcell_angles=unitcell_angles)

    traj.save_dcd('traj_walker'+str(walker_idx)+'.dcd')
    print('DCD file of walker {} is saved'.format(walker_idx))
    wepy_h5.close()



if __name__=='__main__':

    make_dcd_for_walker(walker_idx)
